import 'package:json_annotation/json_annotation.dart';

part 'models.g.dart';

// build file part: https://pub.dev/packages/json_serializable

@JsonSerializable(explicitToJson: true)
class MealsModel {
  List<CategoryModel>? categories;

  MealsModel({this.categories});

  factory MealsModel.fromJson(Map<String, dynamic> json) =>
      _$MealsModelFromJson(json);

  /// Connect the generated [_$PersonToJson] function to the `toJson` method.
  Map<String, dynamic> toJson() => _$MealsModelToJson(this);
}

@JsonSerializable()
class CategoryModel {
  CategoryModel({
    this.idCategory,
    this.strCategory,
    this.strCategoryThumb,
    this.strCategoryDescription,
  });

  String? idCategory;
  String? strCategory;
  String? strCategoryThumb;
  String? strCategoryDescription;

  factory CategoryModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryModelFromJson(json);

  /// Connect the generated [_$PersonToJson] function to the `toJson` method.
  Map<String, dynamic> toJson() => _$CategoryModelToJson(this);
}
