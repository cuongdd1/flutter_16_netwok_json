import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter_16_netwok_json/main.dart';
import 'package:flutter_16_netwok_json/meals/detail_meal.dart';
import 'package:flutter_16_netwok_json/meals/models.dart';

class MealsScreen extends StatefulWidget {
  static const routeName = 'MealsScreen';

  const MealsScreen({Key? key}) : super(key: key);

  @override
  _MealsScreenState createState() => _MealsScreenState();
}

class _MealsScreenState extends State<MealsScreen> {
  List<CategoryModel> _listCategoryMeals = <CategoryModel>[];

  Widget _buildListView(BuildContext context) {
    return ListView.separated(
        separatorBuilder: (context, index) => const Divider(),
        itemCount: _listCategoryMeals.length,
        itemBuilder: (context, index) {
          return _buildCell(index);
        });
  }

  Widget _buildCell(int index) {
    CategoryModel categoryMeal = _listCategoryMeals[index];
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, DetailMeal.routeName,
            arguments: categoryMeal);
      },
      child: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.network(
              '${categoryMeal.strCategoryThumb}',
              width: 120,
              height: 120,
            ),
            Text(
              '${categoryMeal.strCategory}',
              style: const TextStyle(color: Colors.blueAccent, fontSize: 18),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Category Meals'),
      ),
      body: FutureBuilder<MealsModel>(
        future: fetchMeals(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            printMessage(snapshot.hasData.toString());
            _listCategoryMeals = snapshot.data?.categories ?? [];
            return _buildListView(context);
          }
          if (snapshot.hasError) {
            return Text(
              snapshot.hasError.toString(),
            );
          }
          return const ShowLoading();
        },
      ),
    );
  }
}

Future<MealsModel> fetchMeals() async {
  MealsModel meal = MealsModel();
  try {
    Response response =
        await Dio().get('https://themealdb.com/api/json/v1/1/categories.php');
    if (response.statusCode == 200) {
      Map<String, dynamic> data = response.data;

      meal = MealsModel.fromJson(data);
      printMessage(meal.toString());
      return meal;
    } else {
      throw Exception('Failed to load users');
    }
  } catch (e) {
    printMessage(e.toString());
  }
  return meal;
}
