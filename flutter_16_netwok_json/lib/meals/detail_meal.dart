import 'package:flutter/material.dart';
import 'package:flutter_16_netwok_json/meals/models.dart';

class DetailMeal extends StatefulWidget {
  const DetailMeal({Key? key}) : super(key: key);

  static const routeName = 'DetailMeal';

  @override
  _DetailMealState createState() => _DetailMealState();
}

class _DetailMealState extends State<DetailMeal> {
  CategoryModel meal = CategoryModel();

  Widget _buildListView(BuildContext context) {
    return ListView.builder(
      itemCount: 3,
      itemBuilder: (context, index) {
        if (index == 0) {
          return Padding(
            padding: const EdgeInsets.all(16),
            child: Image.network(
                'https://docs.flutter.dev/assets/images/dash/dash-fainting.gif'),
          );
        }
        if (index == 1) {
          return Expanded(
            child: Image.network('${meal.strCategoryThumb}'),
          );
        }
        return ListTile(
          title: Text('${meal.strCategoryDescription}',
          style: const TextStyle(fontSize: 20),),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    meal = ModalRoute.of(context)!.settings.arguments as CategoryModel;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${meal.strCategory}',
        ),
      ),
      body: _buildListView(context),
    );
  }
}
