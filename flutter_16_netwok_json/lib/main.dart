import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter_16_netwok_json/meals/detail_meal.dart';
import 'package:flutter_16_netwok_json/meals/meals_screen.dart';
import 'package:flutter_16_netwok_json/model.dart';
import 'package:flutter_16_netwok_json/slidable/slidable.dart';
import 'package:shimmer/shimmer.dart';

//link study: https://codingwithdhrumil.com/2021/02/dio-flutter-rest-api-example.html

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Isolate Demo';

    return MaterialApp(
      initialRoute: HomeScreen.routeName,
      routes: {
        HomeScreen.routeName: (context) => const HomeScreen(),
        UserScreen.routeName: (context) => const UserScreen(),
        MealsScreen.routeName: (context) => const MealsScreen(),
        SlidableScreen.routeName: (context) => const SlidableScreen(),
        DetailEmployee.routeName: (context) => const DetailEmployee(),
        DetailMeal.routeName: (context) => const DetailMeal(),
      },
      title: appTitle,
      home: const HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  static const routeName = 'HomeScreen';
  const HomeScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: UserScreen(),
    );
  }
}

class UserScreen extends StatefulWidget {

  const UserScreen({Key? key}) : super(key: key);

  static const routeName = 'UserScreen';

  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  List<User> _listUsers = <User>[];
  final TextStyle _styleNormal =
      const TextStyle(fontWeight: FontWeight.bold, fontSize: 22);

  void pushNamedToMeals(BuildContext context) {
    Navigator.pushNamed(context, MealsScreen.routeName);
  }

  Widget _buildListView(BuildContext context) {
    return ListView.separated(
        separatorBuilder: (context, index) => const Divider(),
        itemCount: _listUsers.length,
        itemBuilder: (context, index) {
          return _buildCell(index);
        });
  }

  Widget _buildCell(int index) {
    User user = _listUsers[index];
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, DetailEmployee.routeName, arguments: user);
      },
      child: Container(
        color: Colors.blueAccent,
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              user.name ?? '',
              style: _styleNormal,
            ),
            const SizedBox(height: 5),
            Text(user.email ?? ''),
            const SizedBox(height: 5),
            Text(
              '${user.address?.street} ${user.address?.suite} ${user.address?.city} ${user.address?.zipcode}',
            ),
            const SizedBox(height: 5),
            Text(
              '${user.phone}',
            ),
            const SizedBox(height: 5),
            Text(
              '${user.website}',
            ),
            const SizedBox(height: 5),
            Text(
              '${user.company?.name}',
            ),
            const SizedBox(height: 5),
            Text('${user.company?.catchPhrase}'),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dio Demo'),
        leading: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, SlidableScreen.routeName);
            },
            icon: const Icon(Icons.menu),
          ),
        actions: [
          IconButton(
            onPressed: () {
              pushNamedToMeals(context);
            },
            icon: const Icon(Icons.list),
          )
        ],
      ),
      body: FutureBuilder<List<User>>(
        future: fetchUsers(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            _listUsers = (snapshot.data as List<User>);
            return _buildListView(context);
          } else if (snapshot.hasError) {
            return Center(child: Text('${snapshot.error}'));
          }
          return const ShowLoading();
        },
      ),
    );
  }
}

//Get Users From Api using dio flutter
Future<List<User>> fetchUsers() async {
  List<User> listUsers = <User>[];
  try {
    Response response =
        await Dio().get('https://jsonplaceholder.typicode.com/users');
    if (response.statusCode == 200) {
      var listData = response.data as List;
      listUsers = listData.map((i) => User.fromJSON(i)).toList();
      return listUsers;
    } else {
      throw Exception('Failed to load users');
    }
  } catch (e) {
    printMessage(e.toString());
  }
  return listUsers;
}

void printMessage(String message) {
  if (kDebugMode) {
    print(message);
  }
}

class ShowLoading extends StatelessWidget {
  const ShowLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Expanded(
          child: Shimmer.fromColors(
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
            enabled: true,
            child: ListView.builder(
              itemBuilder: (_, __) => Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 48.0,
                      height: 48.0,
                      color: Colors.white,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            height: 8.0,
                            color: Colors.white,
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 2.0),
                          ),
                          Container(
                            width: double.infinity,
                            height: 8.0,
                            color: Colors.white,
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 2.0),
                          ),
                          Container(
                            width: 40.0,
                            height: 8.0,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              itemCount: 16,
            ),
          ),
        ),
      ],
    );
  }
}


class DetailEmployee extends StatelessWidget {
  const DetailEmployee({Key? key}) : super(key: key);

  static const routeName = 'DetailEmployee';

  @override
  Widget build(BuildContext context) {
    final user = ModalRoute.of(context)!.settings.arguments as User;
    return Scaffold(
      appBar: AppBar(
        title: Text(user.name ?? '',),
      ),
    );
  }
}
